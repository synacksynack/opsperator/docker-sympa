FROM opsperator/apache

# Sympa image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    SMTPCHKURL=https://github.com/nabeken/go-check-smtp/releases/download \
    SMTPCHKVERS=v2020051904

LABEL io.k8s.description="Sympa & Postfix Mailing Lists." \
      io.k8s.display-name="Sympa" \
      io.openshift.expose-services="8080:http,25:smtp,465:smtps" \
      io.openshift.tags="sympa" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-sympa" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="6.2.40"

USER root

COPY config/* /

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Sympa Depenencies" \
    && apt-get -y install --no-install-recommends apt-transport-https wget \
	gnupg2 mariadb-client postgresql-client nullmailer rsyslog locales \
    && echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen \
    && dpkg-reconfigure locales \
    && apt-get install -y shared-mime-info libio-socket-ip-perl krb5-locales \
	libsasl2-modules libhtml-form-perl libhttp-daemon-perl postfix \
	libxml-sax-expat-perl xml-core libfile-nfslock-perl libsoap-lite-perl \
	libcrypt-ciphersaber-perl libmail-dkim-perl libapache2-mod-fcgid \
	libio-socket-inet6-perl libmime-types-perl libauthcas-perl patch \
	libarchive-zip-perl libdbd-mysql-perl libfile-copy-recursive-perl \
	libhtml-format-perl libhtml-stripscripts-parser-perl \
	libio-stringy-perl libmime-charset-perl libmime-encwords-perl \
	libmime-tools-perl libnet-dns-perl libnet-netmask-perl \
	libtemplate-perl libterm-progressbar-perl libxml-libxml-perl \
	libcrypt-openssl-x509-perl libdbd-odbc-perl libdbd-pg-perl \
	libdbd-sqlite3-perl libdbd-sybase-perl libdata-password-perl \
	libfcgi-perl libnet-ldap-perl libintl-perl libmime-lite-html-perl \
	libunicode-linebreak-perl libcgi-fast-perl libio-socket-ssl-perl \
    && echo "# Install Sympa" \
    && apt-get download sympa \
    && dpkg --unpack sympa*.deb \
    && rm /var/lib/dpkg/info/sympa.postinst -f \
    && ( dpkg --configure sympa || echo yay ) \
    && apt-get install -yf \
    && mv /usr/lib/cgi-bin/sympa /usr/lib/cgi-bin/sympa.orig \
    && mv /var/spool/sympa /var/spool/sympa.orig \
    && mv /etc/postfix /etc/postfix.orig \
    && mv /etc/sympa /etc/sympa.orig \
    && mv /var/lib/sympa /var/lib/sympa.orig \
    && mv /docker-sympa.conf /etc/rsyslog.d/ \
    && mv /check_smtp.sh /log_rotate.sh /usr/local/bin/ \
    && mv /nsswrapper.sh /usr/local/bin/sympa-nsswrapper.sh \
    && mv /reset-tls.sh /usr/local/bin/postfix-reset-tls.sh \
    && mkdir /etc/postfix /var/log/postfix \
    && sed -i "s|nonfixed quote|nonfixed disableflowed quote|" \
	/usr/share/sympa/default/mhonarc-ressources.tt2 \
    && sed -i 's|/run/sympa|/run/sympa/pidsubdir|g' \
	/usr/share/sympa/lib/Sympa/Constants.pm \
    && patch -d/ -p0 </auth.patch \
    && patch -d/ -p0 </bulk-spool.patch \
    && patch -d/ -p0 </cgi.patch \
    && patch -d/ -p0 </log.patch \
    && patch -d/ -p0 </send-spool.patch \
    && patch -d/ -p0 </sympa.patch \
    && patch -d/ -p0 </www-tools.patch \
    && ls /usr/lib/sympa/bin/*pl \
	| while read f; \
	    do \
		sed -i -e \
		    '/Check if the UID has correctly been set/,+5d' "$f"; \
	    done \
    && echo "# Configure Apache" \
    && echo "FcgidIPCDir /etc/apache2/tmp/mod_fcgid" \
	>>/etc/apache2/mods-available/fcgid.conf \
    && echo "FcgidProcessTableFile /etc/apache2/tmp/mod_fcgid/fcgid_shm" \
	>>/etc/apache2/mods-available/fcgid.conf \
    && a2enmod fcgid \
    && if uname -m | grep x86_64 >/dev/null; then \
	echo "# Install SMTP Check" \
	&& wget -O /usr/src/check_smtp.tar.gz \
	    "$SMTPCHKURL/$SMTPCHKVERS/go-check-smtp_linux_amd64.tar.gz" \
	&& tar --strip-components=1 -C /usr/src -xzf /usr/src/check_smtp.tar.gz \
	&& mv /usr/src/go-check-smtp /usr/bin/check_smtp \
	&& chown root:root /usr/bin/check_smtp \
	&& chmod 555 /usr/bin/check_smtp \
	&& rm -fr /usr/src/*; \
    fi \
    && for d in /etc/sympa /run /var/spool/sympa /var/log/sympa /var/lib/sympa \
	/etc/apache2/mods-available /etc/apache2/mods-enabled \
	/var/spool/sympa/msg /var/spool/sympa/digest /var/spool/sympa/auth \
	/var/spool/sympa/outgoing /etc/apache2/tmp/mod_fcgid \
	/var/spool/sympa/subscribe /var/spool/sympa/bulk /var/spool/sympa/task \
	/var/spool/sympa/bounce /var/spool/sympa/automatic \
	/var/spool/sympa/topic /var/spool/sympa/viewmail \
	/var/spool/sympa/moderation /var/spool/nullmailer \
	/usr/share/sympa/static_content /usr/lib/cgi-bin/sympa; \
    do \
	mkdir -p "$d"; \
	chown -R g=u $d || echo nevermind; \
	chown -R 1001:0 $d || echo nevermind; \
    done \
    && for f in /etc/aliases /etc/aliases.db /var/log/sympa.log; \
    do \
	touch "$f"; \
	chown g=u $f || echo nevermind; \
	chown 1001:0 $f || echo nevermind; \
    done \
    && echo "# Cleaning Up" \
    && apt-get -y remove --purge apt-transport-https gnupg2 wget patch \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rvf /etc/apache2/ports.conf /usr/share/man /usr/share/doc \
	/var/lib/apt/lists/* /root/.gnupg /etc/rsyslog.d/sympa.conf /*patch \
	/usr/src/* sympa*deb \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT [ "dumb-init", "--", "/entrypoint.sh" ]
USER 1001
