# k8s Sympa

Kubernetes-friendly Sympa image.

Diverts from https://gitlab.com/synacksynack/opsperator/docker-apache

Depends on an OpenLDAP server, such as
https://gitlab.com/synacksynack/opsperator/docker-openldap

Build with:

```
$ make build
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |    Description               | Default                                                               | Inherited From    |
| :------------------------- | ---------------------------- | --------------------------------------------------------------------- | ----------------- |
|  `ADMIN_USER`              | Sympa Admin Users            | `admin0`                                                              |                   |
|  `AUTH_METHOD`             | Sympa Auth Backend           | undef (use `ldap` or `lemon` for LDAP or CAS logins)                  |                   |
|  `APACHE_DOMAIN`           | Apache ServerName            | `example.com`                                                         | opsperator/apache |
|  `APACHE_HTTP_PORT`        | Apache Listen Port           | `8080`                                                                | opsperator/apache |
|  `DKIM_HOST`               | OpenDKIM Host Address        | undef (`127.0.0.1` when starting OpenDKIM itself)                     |                   |
|  `DKIM_PORT`               | OpenDKIM Bind Port           | `12345`                                                               |                   |
|  `DO_SOAP`                 | Sympa SOAP Toggle            | `true`                                                                |                   |
|  `HEADER_FIELDS`           | Sympa Header Fields          | `help,subscribe,unsubscribe,post,owner,archive`                       |                   |
|  `LISTS_LANG`              | Sympa UI Language            | `en`                                                                  |                   |
|  `LISTS_SUBDOMAIN`         | Sympa Lists Subdomain        | `lists` (mails for `$LISTS_SUBDOMAIN.$OPENLDAP_DOMAIN`)               |                   |
|  `MAIL_DOMAIN`             | Sympa Mails Root Domain      | `$OPENLDAP_DOMAIN`                                                    |                   |
|  `MAX_SIZE`                | Sympa Max Message Size       | `15728640`                                                            |                   |
|  `MYHOSTNAME`              | Postfix FQDN                 | `smtp.$LISTS_SUBDOMAIN.$OPENLDAP_DOMAIN`                              |                   |
|  `MYSQL_DATABASE`          | MySQL Database Name          | `sympa`                                                               |                   |
|  `MYSQL_HOST`              | MySQL Database Host          | `127.0.0.1`                                                           |                   |
|  `MYSQL_PASSWORD`          | MySQL Database Password      | undef                                                                 |                   |
|  `MYSQL_PORT`              | MySQL Database Port          | `3306`                                                                |                   |
|  `MYSQL_USER`              | MySQL Database Username      | `sympa`                                                               |                   |
|  `ONLY_TRUST_KUBE_CA`      | Don't trust base image CAs   | `false`, any other value disables ca-certificates CAs                 | opsperator/apache |
|  `OPENLDAP_BASE`           | OpenLDAP Base                | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local`           | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX` | OpenLDAP Bind DN Prefix      | `cn=sympa,ou=services`                                                | opsperator/apache |
|  `OPENLDAP_BIND_PW`        | OpenLDAP Bind Password       | `secret`                                                              | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX` | OpenLDAP Conf DN Prefix      | `cn=lemonldap,ou=config`                                              | opsperator/apache |
|  `OPENLDAP_DOMAIN`         | OpenLDAP Domain Name         | undef                                                                 | opsperator/apache |
|  `OPENLDAP_HOST`           | OpenLDAP Backend Address     | undef                                                                 | opsperator/apache |
|  `OPENLDAP_PORT`           | OpenLDAP Bind Port           | `389` or `636` depending on `OPENLDAP_PROTO`                          | opsperator/apache |
|  `OPENLDAP_PROTO`          | OpenLDAP Proto               | `ldap`                                                                | opsperator/apache |
|  `PASSWORDS_FQDN`          | LTB SelfServicePassword      | undef, prevents registrations if passed                               |                   |
|  `POSTGRES_DB`             | Postgres Database Name       | `sympa`                                                               |                   |
|  `POSTGRES_HOST`           | Postgres Database Host       | `127.0.0.1`                                                           |                   |
|  `POSTGRES_PASSWORD`       | Postgres Database Password   | undef                                                                 |                   |
|  `POSTGRES_PORT`           | Postgres Database Port       | `5432`                                                                |                   |
|  `POSTGRES_USER`           | Postgres Database Username   | `sympa`                                                               |                   |
|  `PUBLIC_PROTO`            | Apache Public Proto          | `http`                                                                | opsperator/apache |
|  `REMOVE_HEADERS`          | Sympa Header Fields          | `Return-Receipt-To,Precedence,X-Sequence,Disposition-Notification-To` |                   |
|  `SITE_TITLE`              | Sympa Site Title             | `Mailing Lists`                                                       |                   |
|  `SMTP_PORT`               | Postfix SMTP Relay Port      | `25`                                                                  |                   |
|  `SMTP_PROTO`              | Postfix SMTP Relay Proto     | `smtp`                                                                |                   |
|  `SMTP_RELAY`              | Postfix SMTP Relay Host      | undef                                                                 |                   |
|  `SPAMD_HOST`              | AntiSpam Milter Host Address | undef                                                                 |                   |
|  `SPAMD_PORT`              | AntiSpam Milter Port         | `1783`                                                                |                   |
|  `SYMPA_WORKERS`           | Sympa Workers to Start       | `task_manager bulk bounced archived sympa_msg`                        |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point           | Description                                                |
| :---------------------------- | ---------------------------------------------------------- |
|  `/certs`                     | Postfix Certificate (optional)                             |
|  `/etc/apache2/sites-enabled` | Sympa Web VirtualHosts                                     |
|  `/etc/apache2/ssl`           | Sympa Web SOAP + SSL configurations                        |
|  `/etc/apache2/tmp`           | Sympa Web FCGI Working Directory                           |
|  `/etc/postfix`               | Postfix Configuration                                      |
|  `/etc/sympa`                 | Sympa Configuration                                        |
|  `/etc/sympa.orig/cookie`     | Sympa Secret Cookie - otherwise dummy value would be set   |
|  `/run/rsyslog`               | Postfix / Rsyslog Socket Directory                         |
|  `/run/sympa`                 | Sympa Mock Pid Directory                                   |
|  `/usr/lib/cgi-bin/sympa`     | Sympa CGI Scripts                                          |
|  `/var/lib/sympa`             | Sympa Lib Directory                                        |
|  `/var/spool/postix`          | Postfix Spool Directories - to share with Sympa            |
|  `/var/spool/sympa`           | Sympa Spool Directories                                    |
