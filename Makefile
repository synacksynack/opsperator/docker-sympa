SKIP_SQUASH?=1
IMAGE=opsperator/sympa
-include Makefile.cust

.PHONY: build
build:
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service config rbac statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "CYRUS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "CYRUS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocpurge
ocpurge: occheck
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true

.PHONY: test
test:
	@@docker rm -f testmysql || true
	@@docker run --name testmysql \
	    -e MYSQL_DATABASE=msdb \
	    -e MYSQL_PASSWORD=mspassword \
	    -e MYSQL_ROOT_PASSWORD=msrootpassword \
	    -e MYSQL_USER=msuser \
	    -p 3306:3306 \
	    -d docker.io/centos/mariadb-102-centos7:latest
	@@mkdir -p volume/slib volume/mail volume/sspool volume/sympa volume/spool volume/rsyslog volume/postfix || echo meh
	@@chmod -R 777 volume || echo warning
	@@docker rm -f testsympaworkers || echo ok
	@@docker rm -f testsympasmtp || echo ok
	@@docker rm -f testsympasyslog || echo ok
	@@docker rm -f testsympaweb || echo ok
	@@msip=`docker inspect testmysql | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	echo "Working with mysql=$$msip"; \
	docker run --name testsympaworkers \
	    --user `id -u` \
	    -e AUTH_METHOD=internal \
	    -e MYSQL_DATABASE=msdb \
	    -e MYSQL_HOST=$$msip \
	    -e MYSQL_PASSWORD=mspassword \
	    -e MYSQL_USER=msuser \
	    -v `pwd`/volume/mail:/etc/mail/sympa \
	    -v `pwd`/volume/postfix:/etc/postfix \
	    -v `pwd`/volume/sympa:/etc/sympa \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -v `pwd`/volume/slib:/var/lib/sympa \
	    -v `pwd`/volume/spool:/var/spool/postfix \
	    -v `pwd`/volume/sspool:/var/spool/sympa \
	    --tmpfs /run/sympa:rw,size=65536k  \
	    -d $(IMAGE) /run-sympa.sh
	@@docker run --name testsympasmtp \
	    --privileged \
	    --user 0 \
	    -e AUTH_METHOD=internal \
	    -v `pwd`/volume/postfix:/etc/postfix \
	    -v `pwd`/volume/sympa:/etc/sympa \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -v `pwd`/volume/spool:/var/spool/postfix \
	    -d $(IMAGE) /run-postfix.sh
	@@docker run --name testsympasyslog \
	    --privileged \
	    --user 0 \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    --tmpfs /var/log/postfix:rw,size=65536k  \
	    -d $(IMAGE) /run-syslog.sh
	@@msip=`docker inspect testmysql | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	echo "Working with mysql=$$msip"; \
	docker run --name testsympaweb \
	    --user `id -u`:0 \
	    -e AUTH_METHOD=internal \
	    -e LISTS_SUBDOMAIN=lists \
	    -e MAIL_DOMAIN=demo.lan \
	    -e MYSQL_DATABASE=msdb \
	    -e MYSQL_HOST=$$msip \
	    -e MYSQL_PASSWORD=mspassword \
	    -e MYSQL_USER=msuser \
	    -v `pwd`/volume/mail:/etc/mail/sympa \
	    -v `pwd`/volume/postfix:/etc/postfix \
	    -v `pwd`/volume/sympa:/etc/sympa \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -v `pwd`/volume/slib:/var/lib/sympa \
	    -v `pwd`/volume/spool:/var/spool/postfix \
	    -v `pwd`/volume/sspool:/var/spool/sympa \
	    --tmpfs /etc/apache2/sites-enabled:rw,uid=`id -u`,size=65536k  \
	    --tmpfs /etc/apache2/tmp:rw,uid=`id -u`,size=16M \
	    --tmpfs /run/sympa:rw,uid=`id -u`,size=65536k  \
	    --tmpfs /usr/lib/cgi-bin/sympa:rw,uid=`id -u`,size=65536k  \
	    -d $(IMAGE) /run-web.sh
