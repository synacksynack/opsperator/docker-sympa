#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/sympa-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	uid=`id -u`
	gid=`id -g`
	if test "$gid" = 0; then
	    gid=$uid
	fi
	echo Setting up nsswrapper mapping $uid to sympa
	(
	    sed -e "s|^postdrop:.*|postdrop:x:$gid:|" \
		/etc/group
	    echo "sympa:x:$gid:"
	) >/tmp/sympa-group
	(
	    cat /etc/passwd
	    echo "sympa:x:$uid:$gid:sympa:/var/lib/sympa:/usr/sbin/nologin"
	) >/tmp/sympa-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/sympa-passwd
    export NSS_WRAPPER_GROUP=/tmp/sympa-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
