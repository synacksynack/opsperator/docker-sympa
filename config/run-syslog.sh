#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

ln -sf /run/rsyslog/dev/log /dev/log
exec rsyslogd -n -f /etc/rsyslog.conf -i /run/rsyslogd.pid
