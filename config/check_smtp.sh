#!/bin/sh

if test -x /usr/bin/check_smtp; then
    MYDOMAIN=${MYDOMAIN:-demo.local}
    if test -z "$MYHOSTNAME"; then
	MYHOSTNAME=smtp.$MYDOMAIN
    fi
    exec /usr/bin/check_smtp -P -F $MYHOSTNAME -H 127.0.0.1 -p 25 -w 2s -c 5s
else
    exec /usr/bin/mailq
fi
