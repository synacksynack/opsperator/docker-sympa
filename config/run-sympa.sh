#!/bin/sh

if test "$DEBUG"; then
    set -x
    LOGLEVEL=${LOGLEVEL:-2}
fi

. /usr/local/bin/sympa-nsswrapper.sh

ADMIN_USER=${ADMIN_USER:-admin0}
AUTH_METHOD=${AUTH_METHOD:-}
LISTS_LANG=${LISTS_LANG:-en}
LISTS_SUBDOMAIN=${LISTS_SUBDOMAIN:-lists}
LOGLEVEL=${LOGLEVEL:-0}
MAX_SIZE=${MAX_SIZE:-15728640}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=sympa,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-openldap}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
if test -z "$HEADER_FIELDS"; then
    HEADER_FIELDS="help,subscribe,unsubscribe,post,owner,archive"
fi
if test -z "$MAIL_DOMAIN"; then
    MAIL_DOMAIN=$OPENLDAP_DOMAIN
fi
if test -z "$LISTS_ADMINS"; then
    for adm in $ADMIN_USER
    do
	if echo "$adm" | grep @ >/dev/null; then
	    LISTS_ADMINS="$LISTS_ADMINS $adm"
	else
	    LISTS_ADMINS="$LISTS_ADMINS $adm@$MAIL_DOMAIN"
	fi
    done
fi
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$LDAP_TLS"; then
    if test "$OPENLDAP_PROTO" = ldaps; then
	LDAP_TLS=ldaps
    else
	LDAP_TLS=none
    fi
fi
if test "$LDAP_TLS" = ldaps; then
    if test -z "$LDAP_SKIP_VERIFY"; then
	LDAP_CA_VERIFY=required
    else
	LDAP_CA_VERIFY=none
    fi
else
    LDAP_CA_VERIFY=none
fi
if test -z "$REMOVE_HEADERS"; then
    REMOVE_HEADERS="Return-Receipt-To,Precedence,X-Sequence,Disposition-Notification-To"
fi
if test -z "$SITE_TITLE"; then
    SITE_TITLE="Mailing Lists"
fi
APACHE_DOMAIN=$LISTS_SUBDOMAIN.$MAIL_DOMAIN

mkdir -p /etc/sympa/sympa /run/sympa/pidsubdir
cpt=0
if test "$POSTGRES_PASSWORD"; then
    POSTGRES_DB=${POSTGRES_DB:-sympa}
    POSTGRES_HOST=${POSTGRES_HOST:-127.0.0.1}
    POSTGRES_PORT=${POSTGRES_PORT:-5432}
    POSTGRES_USER=${POSTGRES_USER:-sympa}
    echo Waiting for Postgres backend ...
    while true
    do
	if echo '\d' | PGPASSWORD="$POSTGRES_PASSWORD" \
		psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach Postgres" >&2
	    exit 1
	fi
	sleep 5
	echo Postgres is ... KO
	cpt=`expr $cpt + 1`
    done
    DB_DATABASE=$POSTGRES_DB
    DB_DRIVER=PostgreSQL
    DB_HOST=$POSTGRES_HOST
    DB_PASS=$POSTGRES_PASSWORD
    DB_PORT=$POSTGRES_PORT
    DB_USER=$POSTGRES_USER
elif test "$MYSQL_PASSWORD"; then
    MYSQL_DATABASE=${MYSQL_DATABASE:-sympa}
    MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
    MYSQL_PORT=${MYSQL_PORT:-3306}
    MYSQL_USER=${MYSQL_USER:-sympa}
    echo Waiting for MySQL backend ...
    while true
    do
	if echo SHOW TABLES | mysql -u "$MYSQL_USER" \
		--password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
		-P "$MYSQL_PORT" "$MYSQL_DATABASE" >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach MySQL" >&2
	    exit 1
	fi
	sleep 5
	echo MySQL is ... KO
	cpt=`expr $cpt + 1`
    done
    DB_DATABASE=$MYSQL_DATABASE
    DB_DRIVER=MySQL
    DB_HOST=$MYSQL_HOST
    DB_PASS=$MYSQL_PASSWORD
    DB_PORT=$MYSQL_PORT
    DB_USER=$MYSQL_USER
elif test -z "$DB_DRIVER"; then
    echo "Critical: missing Database configuration"
    exit 1
fi

if test -s /etc/sympa/sympa/sympa.conf; then
    mvcfg=false
    if test "$RESET_CONFIG"; then
	echo NOTICE: resetting main configuration
	mvcfg=true
    elif ! grep "^listmaster $LIST_ADMINS" /etc/sympa/sympa/sympa.conf >/dev/null; then
	echo NOTICE: resetting sympa admins
	mvcfg=true
    fi
    if $mvcfg; then
	mv /etc/sympa/sympa/sympa.conf /etc/sympa/sympa/sympa.conf.$(date +%s)
    fi
fi
if test -s /etc/sympa/auth.conf; then
    mvcfg=false
    if test "$RESET_CONFIG"; then
	echo NOTICE: resetting auth configuration
	mvcfg=true
    elif test "$AUTH_METHOD" = lemon; then
	if ! grep generic_sso /etc/sympa/auth.conf >/dev/null; then
	    echo NOTICE: resetting auth configuration integrating with LLNG
	    mvcfg=true
	fi
    elif test "$AUTH_METHOD" = ldap; then
	if ! grep ^ldap /etc/sympa/auth.conf >/dev/null; then
	    echo NOTICE: resetting autuh configuration integrating with LDAP
	    mvcfg=true
	fi
    else
	if grep -E '^(ldap|generic_sso)' /etc/sympa/auth.conf >/dev/null; then
	    echo NOTICE: resetting auth configuration, disables integrations
	    mvcfg=true
	fi
    fi
    if $mvcfg; then
	mv /etc/sympa/auth.conf /etc/sympa/auth.conf.$(date +%s)
    fi
fi

if ! test -s /etc/sympa/sympa/sympa.conf; then
    sed -e "s|LISTS_ADMINS|$LISTS_ADMINS|" \
	-e "s|LISTS_DOMAIN|$APACHE_DOMAIN|" \
	-e "s|LISTS_LANG|$LISTS_LANG|" \
	-e "s|LOGLEVEL|$LOGLEVEL|" \
	-e "s|HEADER_FIELDS|$HEADER_FIELDS|" \
	-e "s|MAX_SIZE|$MAX_SIZE|" \
	-e "s|REMOVE_HEADERS|$REMOVE_HEADERS|" \
	-e "s|SQL_DB|$DB_DATABASE|" \
	-e "s|SQL_DRIVER|$DB_DRIVER|" \
	-e "s|SQL_HOST|$DB_HOST|" \
	-e "s|SQL_PASS|$DB_PASS|" \
	-e "s|SQL_PORT|$DB_PORT|" \
	-e "s|SQL_USER|$DB_USER|" \
	-e "s|PUBLIC_PROTO|$PUBLIC_PROTO|" \
	-e "s|SITE_TITLE|$SITE_TITLE|" \
	/sympa.conf >/etc/sympa/sympa/sympa.conf
else
    echo NOTICE: re-using existing sympa/sympa.conf
fi
if ! test -s /etc/sympa/auth.conf; then
    if test "$PASSWORDS_FQDN"; then
	PASSWORDS_URL=$PUBLIC_PROTO://$PASSWORDS_FQDN
    fi
    if test "$AUTH_METHOD" = lemon; then
	if test -z "$LEMON_FQDN"; then
	    LEMON_FQDN=auth.$OPENLDAP_DOMAIN
	fi
	sed -e "s|PASSWORDS_URL|$PASSWORDS_URL|" \
	    -e "s|LEMON_FQDN|$LEMON_FQDN|" \
	    -e "s|LDAP_CA_VERIFY|$LDAP_CA_VERIFY|" \
	    -e "s|LDAP_TLS|$LDAP_TLS|" \
	    -e "s|OPENLDAP_BASE|$OPENLDAP_BASE|" \
	    -e "s|OPENLDAP_BIND_DN|$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE|" \
	    -e "s|OPENLDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
	    -e "s|OPENLDAP_HOST|$OPENLDAP_HOST|" \
	    -e "s|OPENLDAP_PORT|$OPENLDAP_PORT|" \
	    -e "s|OPENLDAP_PROTO|$OPENLDAP_PROTO|" \
	    -e "s|PUBLIC_PROTO|$PUBLIC_PROTO|" \
	    /auth-handler.conf >/etc/sympa/auth.conf
	    #/auth-cas.conf >/etc/sympa/auth.conf
	if test -z "$PASSWORDS_URL"; then
	    sed -i "/authentication_info_url/d" /etc/sympa/auth.conf
	fi
    elif test "$AUTH_METHOD" = ldap; then
	sed -e "s|PASSWORDS_URL|$PASSWORDS_URL|" \
	    -e "s|LDAP_CA_VERIFY|$LDAP_CA_VERIFY|" \
	    -e "s|LDAP_TLS|$LDAP_TLS|" \
	    -e "s|OPENLDAP_BASE|$OPENLDAP_BASE|" \
	    -e "s|OPENLDAP_BIND_DN|$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE|" \
	    -e "s|OPENLDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
	    -e "s|OPENLDAP_HOST|$OPENLDAP_HOST|" \
	    -e "s|OPENLDAP_PORT|$OPENLDAP_PORT|" \
	    -e "s|OPENLDAP_PROTO|$OPENLDAP_PROTO|" \
	    -e "s|PUBLIC_PROTO|$PUBLIC_PROTO|" \
	    /auth-ldap.conf >/etc/sympa/auth.conf
	if test -z "$PASSWORDS_URL"; then
	    sed -i "/authentication_info_url/d" /etc/sympa/auth.conf
	fi
    else
	cat <<EOF >/etc/sympa/auth.conf
user_table
    regexp .*
EOF
    fi
else
    echo NOTICE: re-using existing sympa/auth.conf
fi
for f in cookie edit_list.conf topics.conf
do
    if ! test -s /etc/sympa/$f; then
	if test -s /etc/sympa.orig/$f; then
	    cat /etc/sympa.orig/$f >/etc/sympa/$f
	fi
    fi
done
for d in create_list_templates custom_actions custom_conditions data_sources \
    general_task_models mail_tt2 scenari search_filters task_models tasks \
    wws_templates
do
    if ! test -d /etc/sympa/$d; then
	if test -d /etc/sympa.orig/$d; then
	    cp -rf /etc/sympa.orig/$d /etc/sympa/$d
	fi
    fi
done
for d in arc bounce css list_data pictures wwsarchive
do
    if ! test -d /var/lib/sympa/$d; then
	if test -d /var/lib/sympa.orig/$d; then
	    cp -rf /var/lib/sympa.orig/$d /var/lib/sympa/$d
	fi
    fi
done

if ! test -s /etc/sympa/cookie; then
    echo WARNING: initializing site with dummy cookie
    echo 987654321 >/etc/sympa/cookie
fi
if ! test -s /etc/sympa/cookies.history; then
    cat /etc/sympa/cookie >/etc/sympa/cookies.history
fi
for f in sympa/aliases.sympa.postfix sympa/aliases.sympa.postfix.db \
	mail/sympa/aliases mail/sympa/aliases.db
do
    if ! test -f /etc/$f; then
	touch /etc/$f
    fi
done

mkdir -p /etc/sympa/$APACHE_DOMAIN
if ! test -s /etc/sympa/$APACHE_DOMAIN/robots.conf; then
    touch /etc/sympa/$APACHE_DOMAIN/robots.conf
fi

while ! test -d /var/spool/postfix/maildrop
do
    echo waiting for postfix maildrop directory...
    sleep 10
done
echo maildrop ready

IMGVERSION=$(dpkg -l| awk '/sympa[ \t]/{print $3;exit;}' | cut -d~ -f1)
if test -s /etc/sympa/my.version; then
    INSTALLED=$(tail -1 /etc/sympa/my.version)
fi

for d in bounce bulk digest msg outgoing task tmp
do
    mkdir -p /var/spool/sympa/$d
done
if ! test "$INSTALLED" = "$IMGVERSION"; then
    perl -e \
	"use lib '/usr/share/sympa/lib'; use Sympa::Constants; print Sympa::Constants::VERSION;" \
	>/etc/sympa/data_structure.version
    #echo $IMGVERSION >/etc/sympa/data_structure.version
    if ! /usr/lib/sympa/bin/sympa.pl --upgrade_config_location; then
	echo WARNING: failed upgrading config location
    fi
    if ! /usr/lib/sympa/bin/sympa.pl --health_check; then
	echo WARNING: failed running upgrade health check
    fi
    if ! /usr/lib/sympa/bin/sympa.pl --upgrade; then
	echo CRITICAL: failed running upgrade
    else
	if ! /usr/share/sympa/bin/upgrade_bulk_spool.pl; then
	    echo WARNING: failed running upgrade bulk spool
	fi
	if ! /usr/share/sympa/bin/upgrade_send_spool.pl; then
	    echo WARNING: failed running upgrade send spool
	fi
	echo $IMGVERSION >/etc/sympa/my.version
	echo "NOTICE: done upgrading from $INSTALLED to $IMGVERSION"
    fi
else
    echo "NOTICE: up-to-date ($IMGVERSION)"
fi

while ! test -e /run/rsyslog/dev/log
do
    echo waiting for rsyslogd ...
    sleep 10
done
echo rsyslogd ready

while ! test -s /etc/postfix/main.cf
do
    echo waiting for postfix ...
    sleep 10
done
echo postfix ready

if ! test -s /etc/sympa/aliases.sympa.postfix; then
    sed -e "s|LISTS_DOMAIN|$APACHE_DOMAIN|" /aliases.postfix >/etc/sympa/aliases.sympa.postfix
fi
postalias hash:/etc/sympa/aliases.sympa.postfix
/usr/lib/sympa/bin/sympa_newaliases.pl

if test -z "$SYMPA_WORKERS"; then
    SYMPA_WORKERS="task_manager bulk bounced archived sympa_msg"
fi

start_worker()
{
    echo up >/run/sympa/pidsubdir/$1.mockpid
    /usr/lib/sympa/bin/$1.pl --foreground
    rm -f run/sympa/pidsubdir/$1.mockpid
}

for worker in $SYMPA_WORKERS
do
    echo === Starting $worker ===
    ( start_worker $worker ) &
done

while :
do
    sleep 30
    for worker in $SYMPA_WORKERS
    do
	if ! test -s /run/sympa/pidsubdir/$worker.mockpid; then
	    echo === Worker $worker is dead, restarting ===
	    ( start_worker $worker ) &
	fi
    done
done
