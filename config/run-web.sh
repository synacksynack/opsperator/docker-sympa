#!/bin/sh

if test "$DEBUG"; then
    set -x
    #APACHE_LOG_LEVEL=debug
    APACHE_LOG_LEVEL=perl:debug
else
    APACHE_LOG_LEVEL=notice
fi

. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
DO_SOAP=${DO_SOAP:-true}
LISTS_SUBDOMAIN=${LISTS_SUBDOMAIN:-lists}
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
if test -z "$MAIL_DOMAIN"; then
    MAIL_DOMAIN=$OPENLDAP_DOMAIN
fi

mkdir -p /etc/apache2/ssl /etc/apache2/tmp/mod_fcgid /etc/apache2/sites-enabled
APACHE_DOMAIN=$LISTS_SUBDOMAIN.$MAIL_DOMAIN
export APACHE_DOMAIN
export APACHE_HTTP_PORT
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

while ! test -s /etc/sympa/my.version
do
    echo Waiting for sympa site init ...
    sleep 5
done
echo sympa ready

for f in sympa_soap_server.fcgi wwsympa.fcgi
do
    if ! test -s /usr/lib/cgi-bin/sympa/$f; then
	cat /usr/lib/cgi-bin/sympa.orig/$f >/usr/lib/cgi-bin/sympa/$f
	chmod 0775 /usr/lib/cgi-bin/sympa/$f
    fi
done
for f in sympa_soap_server-wrapper.fcgi wwsympa-wrapper.fcgi
do
    if ! test -s /usr/lib/cgi-bin/sympa/$f; then
	cat /usr/lib/cgi-bin/sympa.orig/$f >/usr/lib/cgi-bin/sympa/$f
	chmod 6775 /usr/lib/cgi-bin/sympa/$f
    fi
done

if test "$DO_SOAP" = true; then
    if test -z "$ALLOW_SOAP"; then
	ALLOW_SOAP=127.0.0.1
    fi
    for h in $ALLOW_SOAP
    do
	echo "Allow from $h"
    done >>/etc/apache2/ssl/allow-soap.conf
fi

cat <<EOF >/etc/apache2/mods-available/fcgid.conf
<IfModule mod_fcgid.c>
    FcgidConnectTimeout ${SYMPA_FCGID_CONNECT_TIMEOUT:-20}
    FcgidMaxProcesses ${SYMPA_FCGID_MAX_PROCESSES:-1000}
    FcgidMaxRequestLen ${LEMON_FCGID_MAX_REQUEST_LEN:-131072}
    FcgidIOTimeout ${SYMPA_FCGID_IO_TIMEOUT:-40}

    <IfModule mod_mime.c>
	AddHandler fcgid-script .fcgi
    </IfModule>
</IfModule>
FcgidIPCDir /etc/apache2/tmp/mod_fcgid
FcgidProcessTableFile /etc/apache2/tmp/mod_fcgid/fcgid_shm
EOF

if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    echo Generating Sympa VirtualHost Configuration
    (
	if grep ^generic_sso /etc/sympa/auth.conf >/dev/null; then
	    cat <<EOF
PerlOptions +GlobalRequest
PerlModule Lemonldap::NG::Handler::ApacheMP2
EOF
	fi
	cat <<EOF
<VirtualHost *:$APACHE_HTTP_PORT>
    ServerName $APACHE_DOMAIN
    CustomLog /dev/stdout modremoteip
    Include "/etc/apache2/$SSL_INCLUDE.conf"
    LogLevel $APACHE_LOG_LEVEL
    DocumentRoot /var/www/html/
    RewriteEngine on
    RewriteCond %{REQUEST_METHOD} ^(TRACE|TRACK)
    RewriteRule .* - [F]
    RewriteRule ^/?\$ /sympa [R=301]
    SetEnvIfNoCase Auth-User "(.*)" HTTP_AUTH_USER=\$1
    SetEnvIfNoCase Mail "(.*)" HTTP_MAIL=\$1
    Alias /favicon.ico /usr/share/sympa/static_content/icons/favicon_sympa.png
    Alias /static-sympa /usr/share/sympa/static_content
EOF
	if grep ^generic_sso /etc/sympa/auth.conf >/dev/null; then
	    cat <<EOF
    PerlHeaderParserHandler Lemonldap::NG::Handler::ApacheMP2
EOF
	fi
	cat <<EOF
    <Location /static-sympa>
	Require all granted
    </Location>
    Alias /css-sympa /var/lib/sympa/css
    <Location /css-sympa>
	Require all granted
    </Location>
    Alias /pictures-sympa /var/lib/sympa/static_content/pictures
    <Location /pictures-sympa>
	Require all granted
    </Location>
    ScriptAlias /wws /usr/lib/cgi-bin/sympa/wwsympa-wrapper.fcgi
    <Location /wws>
	Require all granted
    </Location>
    ScriptAlias /sympa /usr/lib/cgi-bin/sympa/wwsympa-wrapper.fcgi
    <Directory /usr/lib/cgi-bin/sympa>
	Require all granted
    </Directory>
    <Location /sympa>
	Require all granted
    </Location>
EOF
    if test "$DO_SOAP" = true; then
	    cat <<EOF
    ScriptAlias /sympasoap /usr/lib/cgi-bin/sympa/sympa_soap_server-wrapper.fcgi
    <Location /sympasoap>
	SetHandler fcgid-script
	Order deny,allow
	Deny from all
	Include "/etc/apache2/ssl/allow-soap.conf"
    </Location>
EOF
	fi
	cat <<EOF
    <Files ~ "\.(cgi|shtml|phtml|php3?)$">
	SSLOptions +StdEnvVars
    </Files>
    <Directory "/srv/www/cgi-bin">
	SSLOptions +StdEnvVars
    </Directory>
</VirtualHost>
EOF
    ) >/etc/apache2/sites-enabled/003-sympa.conf
fi
if ! grep ^generic_sso /etc/sympa/auth.conf >/dev/null; then
    export APACHE_IGNORE_OPENLDAP=true
else
    export AUTH_METHOD=lemon
fi

set -- /usr/sbin/apache2ctl -D FOREGROUND
. /run-apache.sh
