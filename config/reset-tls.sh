ONLY_TRUST_KUBE_CA=${ONLY_TRUST_KUBE_CA:-false}
RESET_TLS=${RESET_TLS:-false}

should_rehash=false
for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
do
    if test -s $f; then
	if ! test "$ONLY_TRUST_KUBE_CA" = false; then
	    if test -d /etc/pki/ca-trust/source/anchors; then
		if test -z "$had_reset"; then
		    rm -f /etc/pki/tls/cert.pem \
			/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
		    had_reset=true
		fi
		cat $f >>/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
		cat $f >>/etc/pki/tls/cert.pem
	    else
		if test -z "$had_reset"; then
		    find /etc/ssl/certs -type f -delete
		    find /etc/ssl/certs -type l -delete
		    find /usr/share/ca-certificates -type f -delete
		    had_reset=true
		fi
		d=`echo $f | sed 's|/|-|g'`
		if ! cat $f >/usr/share/ca-certificates/kube$d; then
		    echo WARNING: failed installing $f certificate authority >&2
		else
		    should_rehash=true
		fi
	    fi
	else
	    if test -d /etc/pki/ca-trust/source/anchors; then
		dir=/etc/pki/ca-trust/source/anchors
	    else
		dir=/usr/local/share/ca-certificates
	    fi
	    d=`echo $f | sed 's|/|-|g'`
	    if ! test -s $dir/kube$d; then
		if ! cat $f >$dir/kube$d; then
		    echo WARNING: failed installing $f certificate authority >&2
		else
		    should_rehash=true
		fi
	    fi
	fi
    fi
done

if $should_rehash; then
    if test -d /etc/pki/ca-trust/source/anchors; then
	if ! update-ca-trust; then
	    echo WARNING: failed updating trusted certificate authorities >&2
	fi
    elif ! update-ca-certificates; then
	echo WARNING: failed updating trusted certificate authorities >&2
    fi
fi
unset should_rehash had_reset dir

HAS_TLS=false
if test -s /etc/postfix/ssl/server.crt -a \
	-s /etc/postfix/ssl/server.key -a \
	"$RESET_TLS" = false; then
    echo Skipping Postfix SSL configuration - already initialized
    HAS_TLS=true
elif test -s /certs/tls.key -a -s /certs/tls.crt; then
    echo Initializing Postfix SSL configuration
    if ! test -s /certs/ca.crt; then
	if ! test -s /run/secrets/kubernetes.io/serviceaccount/ca.crt; then
	    cat <<EOT >&2
WARNING: Looks like there is no CA chain defined!
	 assuming it is not required or otherwise included in server
	 certificate definition
EOT
	    rm -f /etc/postfix/ssl/ca.crt
	    touch /etc/postfix/ssl/ca.crt
	else
	    cat /run/secrets/kubernetes.io/serviceaccount/ca.crt \
		>/etc/postfix/ssl/ca.crt
	fi
    else
	cat /certs/ca.crt >/etc/postfix/ssl/ca.crt
    fi
    cat /certs/tls.crt >/etc/postfix/ssl/server.crt
    cat /certs/tls.key >/etc/postfix/ssl/server.key
    chmod 0640 /etc/postfix/ssl/server.key
    HAS_TLS=true
fi
