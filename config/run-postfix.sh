#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

ln -sf /run/rsyslog/dev/log /dev/log

if ! ls /etc/postfix/*cf >/dev/null 2>&1; then
    cp -rv /etc/postfix.orig/* /etc/postfix/
fi

mkdir -p /etc/postfix/ssl /var/log/postfix /etc/postfix/sasl

. /usr/local/bin/postfix-reset-tls.sh

ADMIN_USER=${ADMIN_USER:-admin0}
DKIM_HOST=${DKIM_HOST:-}
DKIM_PORT=${DKIM_PORT:-12345}
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
SPAMD_HOST=${SPAMD_HOST:-}
SPAMD_PORT=${SPAMD_PORT:-1783}
if test -z "$MAIL_DOMAIN"; then
    MAIL_DOMAIN=$OPENLDAP_DOMAIN
fi
LISTS_SUBDOMAIN=${LISTS_SUBDOMAIN:-lists}
APACHE_DOMAIN=$LISTS_SUBDOMAIN.$MAIL_DOMAIN
HAS_TLS=false
if test -s /etc/postfix/ssl/server.crt -a \
	-s /etc/postfix/ssl/server.key -a \
	"$RESET_SSL" = false; then
    echo Skipping Postfix SSL configuration - already initialized
    HAS_TLS=true
elif test -s /certs/tls.key -a -s /certs/tls.crt; then
    echo Initializing Postfix SSL configuration
    if ! test -s /certs/ca.crt; then
	if ! test -s /run/secrets/kubernetes.io/serviceaccount/ca.crt; then
	    cat <<EOT >&2
WARNING: Looks like there is no CA chain defined!
	 assuming it is not required or otherwise included in server
	 certificate definition
EOT
	    rm -f /etc/postfix/ssl/ca.crt
	    touch /etc/postfix/ssl/ca.crt
	else
	    cat /run/secrets/kubernetes.io/serviceaccount/ca.crt \
		>/etc/postfix/ssl/ca.crt
	fi
    else
	cat /certs/ca.crt >/etc/postfix/ssl/ca.crt
    fi
    cat /certs/tls.crt >/etc/postfix/ssl/server.crt
    cat /certs/tls.key >/etc/postfix/ssl/server.key
    chmod 0640 /etc/postfix/ssl/server.key
    HAS_TLS=true
fi

if test -z "$MYHOSTNAME"; then
    MYHOSTNAME=smtp.$APACHE_DOMAIN
fi
if test "$SPAMD_HOST" -a "$DKIM_HOST"; then
    MILTERS="inet:$SPAMD_HOST:$SPAMD_PORT inet:$DKIM_HOST:$DKIM_PORT"
elif test "$SPAMD_HOST"; then
    MILTERS="inet:$SPAMD_HOST:$SPAMD_PORT"
elif test "$DKIM_HOST"; then
    MILTERS="inet:$DKIM_HOST:$DKIM_PORT"
fi

if test -z "$DONT_GENERATE_CONF"; then
    if test -s /etc/postfix/master.cf; then
	mv /etc/postfix/master.cf /etc/postfix/master.cf.old
    fi
    cat /master.cf >/etc/postfix/master.cf
    if test -s /etc/postfix/main.cf; then
	mv /etc/postfix/main.cf /etc/postfix/main.cf.old
    fi
    MAXSIZE=${MAXSIZE:-0}
    if test -z "$MYNETWORKS"; then
	MYNETWORKS=127.0.0.1/32
    fi
    sed -e "s|SMTP_FQDN|$MYHOSTNAME|" \
	-e "s|SMTP_MAXSIZE|$MAXSIZE|" \
	-e "s|SMTP_MYNETWORKS|$MYNETWORKS|" \
	/main.cf >/etc/postfix/main.cf
    if ! $HAS_TLS; then
	sed -i "/smtp.*tls/d" /etc/postfix/main.cf
    fi
fi

if test -L /etc/postfix/makedefs.out; then
    rm -f /etc/postfix/makedefs.out
fi
if ! test -s /etc/postfix/makedefs.out; then
    cat /usr/share/postfix/makedefs.out >/etc/postfix/makedefs.out
fi
for f in etc/ssl/certs/ca-certificates.crt etc/services etc/resolv.conf \
    etc/hosts etc/localtime etc/nsswitch.conf etc/host.conf
do
    b=`dirname $f`
    if ! test -d /var/spool/postfix/$b; then
	mkdir /var/spool/postfix/$b
    fi
    cat /$f >/var/spool/postfix/$f
done
if ! test -s /etc/postfix/restricted_senders; then
    cat <<EOF >/etc/postfix/restricted_senders
sexyfun.net REJECT  521
tnt15.nyc3.da.uu.net REJECT  550
EOF
    postmap /etc/postfix/restricted_senders
fi
if test "$SMTP_RELAY"; then
    SMTP_PROTO=${SMTP_PROTO:-smtp}
    if test "$SMTP_PROTO" = smtps; then
	SMTP_PORT=${SMTP_PORT:-465}
	RELAY_WITH=relay-smtps
    else
	SMTP_PORT=${SMTP_PORT:-25}
	RELAY_WITH=smtp
    fi
    cat <<EOF
$APACHE_DOMAIN local
* $RELAY_WITH:$SMTP_RELAY:$SMTP_PORT
EOF
else
    cat <<EOF
$APACHE_DOMAIN local
* smtp
EOF
fi >/etc/postfix/transport
postmap /etc/postfix/transport

if echo "$ADMIN_USER" | grep @ >/dev/null; then
    sed "s|MYUSER|$ADMIN_USER|" \
	/aliases
else
    sed "s|MYUSER|$ADMIN_USER@$MAIL_DOMAIN|" \
	/aliases
fi >/etc/aliases
postalias hash:/etc/aliases

while ! test -e /run/rsyslog/dev/log
do
    echo waiting for rsyslogd ...
    sleep 3
done
echo rsyslogd ready

while ! test -s /etc/sympa/sympa/sympa.conf
do
    echo waiting for sympa configuration ...
    sleep 3
done
echo sympa initialized

SYMPA_GID=$(stat /etc/sympa/sympa/sympa.conf -c %g)
if test "$SYMPA_GID" -gt 0; then
    sed -i "s|postdrop:.*|postdrop:x:$SYMPA_GID:|" /etc/group
fi
/usr/lib/postfix/configure-instance.sh -
mkdir -p /var/spool/postfix/maildrop /var/spool/postfix/public
test -e /var/spool/postfix/public/pickup \
    || mkfifo /var/spool/postfix/public/pickup
test -e /var/spool/postfix/public/qmgr \
    || mkfifo /var/spool/postfix/public/qmgr
chown -R postfix:postdrop /var/spool/postfix/maildrop \
    /var/spool/postfix/public
chmod 3730 /var/spool/postfix/maildrop /var/spool/postfix/public
chmod 0644 /var/spool/postfix/public/pickup \
    /var/spool/postfix/public/qmgr

if test "$MILTERS"; then
    if grep ^smtpd_milters /etc/postfix/main.cf >/dev/null; then
	sed -i "s|^smtpd_milters.*|smtpd_milters = $MILTERS|" \
	    /etc/postfix/main.cf
    else
	echo "smtpd_milters = $MILTERS" >>/etc/postfix/main.cf
    fi
    if grep ^non_smtpd_milters /etc/postfix/main.cf >/dev/null; then
	sed -i "s|^non_smtpd_milters.*|non_smtpd_milters = $MILTERS|" \
	    /etc/postfix/main.cf
    else
	echo "non_smtpd_milters = $MILTERS" >>/etc/postfix/main.cf
    fi
    if grep ^milter_default_action /etc/postfix/main.cf >/dev/null; then
	sed -i 's|^milter_default_action.*|milter_default_action = accept|' \
	    /etc/postfix/main.cf
    else
	echo milter_default_action = accept >>/etc/postfix/main.cf
    fi
    if grep ^milter_protocol /etc/postfix/main.cf >/dev/null; then
	sed -i 's|^milter_protocol.*|milter_protocol = 6|' \
	    /etc/postfix/main.cf
    else
	echo milter_protocol = 6 >>/etc/postfix/main.cf
    fi
else
    if grep ^smtpd_milters /etc/postfix/main.cf >/dev/null; then
	sed -i "/^smtpd_milters.*/d" /etc/postfix/main.cf
    fi
    if grep ^non_smtpd_milters /etc/postfix/main.cf >/dev/null; then
	sed -i "/^non_smtpd_milters.*/d" /etc/postfix/main.cf
    fi
    if grep ^milter_default_action /etc/postfix/main.cf >/dev/null; then
	sed -i '/^milter_default_action.*/d' /etc/postfix/main.cf
    fi
fi

while ! test -s /etc/sympa/aliases.sympa.postfix.db
do
    echo waiting for sympa aliases ...
    sleep 10
done
echo sympa ready

exec postfix start-fg
